import { createRouter, createWebHistory } from 'vue-router'

import BerandaIsai from '../components/BerandaIsai.vue'
import BantuanIsai from '../components/BantuanIsai.vue'
import MapIsai from '../components/MapIsai.vue'
import AdminIsai from '../components/AdminIsai.vue'
import halAdmin from '../components/halAdmin.vue'
import GrafikIsai from '../components/GrafikIsai.vue'
import tabelAdmin from '../components/tabelAdmin.vue'
import KalkuIsai from '../components/KalkuIsai.vue'
import RiwayatIsai from '../components/RiwayatIsai'

const routes =[
    {
        path: '/',
        name: 'beranda',
        component: BerandaIsai
    },

    {
        path: '/map',
        name: 'map',
        component: MapIsai
    },

    {
        path: '/bantuan',
        name: 'bantuan',
        component: BantuanIsai
    },

    {
        path: '/admin',
        name: 'admin',
        component: AdminIsai
    },

    {
        path: '/grafik',
        name: 'grafik',
        component: GrafikIsai
    },

    {
        path: '/halAdmin',
        name: 'halAdmin',
        component: halAdmin

    },

    {
        path: '/tabelAdmin',
        name: 'tabelAdmin',
        component: tabelAdmin
    },

    {
        path: '/kalku',
        name: 'kalku',
        component: KalkuIsai
    },

    {
        path: '/RiwayatIsai/:id',
        name: 'RiwayatIsai',
        component: RiwayatIsai
    }
]


const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, _from, next)=>{
    const isAuthenticated = JSON.parse(localStorage.getItem('authenticated'));
    setTimeout(() => {
      localStorage.removeItem('authenticated');
    }, 3600000);
    if(to.name !== "beranda" && to.name !== "bantuan" && to.name !== "map" && to.name !== "grafik" && to.name !== "kalku" && to.name !== "RiwayatIsai" && to.name !== "admin" && !isAuthenticated) next({name: "admin"});
    else if(to.name == "admin" && isAuthenticated) next({name: "tabelAdmin"});
    
    else next();
  })
export default router
