export const datatren = {
    type: "bar",
    data: {
      labels: null,
      datasets: [
        {
          label: "Nilai karbon tanaman",
          data: null,
          borderColor: "#47b784",
          borderWidth: 3,
          fill: false
        },
        {
          label: "Nilai karbon tanah",
          data: null,
          borderColor: " #964B00",
          borderWidth: 3,
          fill: false
        }
      ]
    },
    options: {
      responsive: true,
      lineTension: 1,
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              padding: 25
            }
          }
        ]
      }
    }
  };
  
  export default datatren;