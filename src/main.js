import { createApp } from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import { loadFonts } from './plugins/webfontloader'
import router from './router'
import { initializeApp } from "firebase/app"; 
// TODO: Add SDKs for Firebase products that you want to use 
// https://firebase.google.com/docs/web/setup#available-libraries 
 
// Your web app's Firebase configuration 
const firebaseConfig = { 
  apiKey: "AIzaSyCu7U7_ef9atqas8mYp80OZUlSv1WBe6p0",
  authDomain: "kedaireka2-38499.firebaseapp.com",
  projectId: "kedaireka2-38499",
  storageBucket: "kedaireka2-38499.appspot.com",
  messagingSenderId: "1062869095783",
  appId: "1:1062869095783:web:738d8bf1fc8dcec31b8bc2",
  measurementId: "G-CZ32120WHE"
}; 
 
// Initialize Firebase 
initializeApp(firebaseConfig); 

loadFonts()

createApp(App).use(router)
  .use(vuetify)
  .mount('#app')
