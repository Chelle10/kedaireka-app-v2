const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  // devServer: { proxy: 'https://8dkru25xy8.execute-api.us-east-1.amazonaws.com/dev/'},
  devServer: { proxy: 'https://sk695s0owe.execute-api.us-east-1.amazonaws.com/dev/'},
  transpileDependencies: true,

  pluginOptions: {
    vuetify: {
			// https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vuetify-loader
		}
    // fallback: {
    //   "crypto": require.resolve("crypto-browserify")
    //  }  
  }
})
